import java.util.Random;


public class GGModel 
{
	private int[] numbers;

	private final int MAX = 10000;
	private final int MIN = 1;
	private int guesses;
	private int userGuesses;
	private int rand;
	private int hi;
	private int lo;
	private int lastGuess;
	
	private Random gen;
	
	public GGModel()
	{
		guesses = 0;
		userGuesses = 0;
		initArray();
		hi = MAX;
		lo = MIN;
		gen = new Random();
	}
	
	// creates the array of numbers to guess from 
	public void initArray()
	{
		numbers = new int[MAX];
		for (int i = MIN; i < MAX; i++)
		{
			numbers[i - 1] = i;
		}
	}
	
	// returns the mid point of hi and lo
	public int guess(int hi, int lo)
	{	
		guesses++;
		lastGuess = lo + (hi - lo) / 2;
		return lo + (hi - lo) / 2;
	}
	
	// returns whether or not the guess is correct
	public boolean checkGuess(String guess)
	{
		return (guess.equals(Integer.toString(rand)));
	}
	
	public void incrementUserGuesses()
	{
		userGuesses++;
	}
	
	public void setRandomInt()
	{
		rand = gen.nextInt(10000);
	}
	
	public void setHi(int num)
	{
		this.hi = num;
	}
	
	public void setLo(int num)
	{
		this.lo = num;
	}
	
	public int getUserGuesses()
	{
		return userGuesses;
	}
	
	public int getRandomInt()
	{
		return rand;
	}
	
	public int getHi()
	{
		return hi;
	}
	
	public int getLastGuess()
	{
		return lastGuess;
	}
	
	public int getLo()
	{
		return lo;
	}
	
	public int getGuesses()
	{
		return guesses;
	}
}
