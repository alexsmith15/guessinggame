public class GuessingGame 
{
	GGController game;
	
	public GuessingGame()
	{
		game = new GGController(new GGModel(), new GGView());
	}
	
	public static void main(String[] args)
	{
		new GuessingGame();
	}
}
