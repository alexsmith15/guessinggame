import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class GGController implements ActionListener
{
	GGModel model;
	GGView view;
	public GGController(GGModel model, GGView view)
	{
		this.model = model;
		this.view = view;
		view.addActionListeners(this);
	}
	public void actionPerformed(ActionEvent e)
	{
		String command = e.getActionCommand();
		if (command.equals("Ready"))
		{
			view.initGame();
			view.setText("Is your number " + model.guess(model.getHi(), model.getLo()) + "?");
		}
		
		if (command.equals("Correct!"))
		{
			view.setText("Wohoo! The Computer found your number in " + model.getGuesses() + " guesses!");
			view.initEndGame();
		}
		
		if (command.equals("Higher"))
		{
			model.setLo(model.getLastGuess());
			view.setText("How about " + model.guess(model.getHi(), model.getLo()) + "?");
		}
		
		if (command.equals("Lower"))
		{
			model.setHi(model.getLastGuess());
			view.setText("What about " + model.guess(model.getHi(), model.getLo()) + "?");
		}
		
		if (command.equals("Continue"))
		{
			model.setRandomInt();
			view.initUserGame();
		}
		
		if (command.equals("End"))
		{
			view.getFrame().setVisible(false);
		}
		
		if (command.equals("Guess"))
		{
		try
		{
			// prevents user from inputting decimals
			if (view.getGuess().contains("."))
			{
				view.setText("Don't use a decimal!");
			}
			
			// prevents crash by using a space
			else if (view.getGuess().contains(" "))
			{
				view.setText("Don't use spaces!");
			}
			
			// Limits user to guessing within 0 and 10000
			else if (Integer.decode(view.getGuess()) >= 10000 || Integer.decode(view.getGuess()) <= 0) 
			{
				view.setText("Your guess must be between 0 and 10000");
			}
			
			else
			{
				model.incrementUserGuesses();
				view.setGuesses("Guesses: " + model.getUserGuesses());

				if (model.checkGuess(view.getGuess()) && model.getUserGuesses() < model.getGuesses())
				{
					view.setText("YOU BEAT THE COMPUTER. AMAZING!!!");
				}
				
				else if (model.checkGuess(view.getGuess()))
				{
					view.setText("Wow you did it");
				}
				
				else if (model.getUserGuesses() > 50)
				{
					view.setText("JUST STOP. You don't even get hints anymore");
				}
				
				else
				{
					if (Integer.decode(view.getGuess()) > model.getRandomInt())
					{
						if (model.getUserGuesses() > 20)
						{
							view.setText("Really? You're terrible at this. It's lower");
						}
						
						else if (model.getUserGuesses() >= model.getGuesses())
						{
							view.setText("WRONG. It's lower than that, and the computer is beating you");
						}
						
						else
						{
							view.setText("WRONG. It's lower than that");
						}
					}
					
					else
					{
						if (model.getUserGuesses() > 20)
						{
							view.setText("Really? You're terrible at this. It's higher");
						}
						
						else if (model.getUserGuesses() >= model.getGuesses())
						{
							view.setText("WRONG. It's higher than that, and the computer is beating you");
						}
						
						else
						{
							view.setText("WRONG. It's higher than that");
						}
					}
				}
			}
		}catch(NumberFormatException exception){view.setText("Type digits not letters!");}
	
		}
	}
}
