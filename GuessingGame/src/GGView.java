import java.awt.GridLayout;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class GGView
{
	private final int FRAME_WIDTH = 400; 
	private final int FRAME_HEIGHT = 100;
	private JFrame frame;
	private JPanel contentPane, titlePanel, buttonPanel, textPanel;
	private JButton enterBtn, higherBtn, contBtn, endBtn, lowerBtn, correctBtn, guessBtn;
	private JLabel textLabel, numGuesses;
	private JTextField textBox;
	
	public GGView()
	{
		initGUI();
	}
	
	public void initGUI()
	{
		frame = new JFrame("Guessing Game");
		frame.setContentPane(initPanels());
		frame.setSize(FRAME_WIDTH, FRAME_HEIGHT);
		frame.setVisible(true);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
	public JPanel initPanels()
	{
		contentPane = new JPanel();
		contentPane.setLayout(null);
		
		titlePanel = new JPanel();
		titlePanel.setLayout(null);
		titlePanel.setLocation(0, 0);
		titlePanel.setSize(FRAME_WIDTH, 20);
		contentPane.add(titlePanel);

		
		textLabel = new JLabel("Think of a number between 0 and 10000");
		textLabel.setHorizontalAlignment(0);
		textLabel.setLocation(0, 0);
		textLabel.setSize(FRAME_WIDTH, 20);
		titlePanel.add(textLabel);
		
		buttonPanel = new JPanel(new GridLayout(1, 3));
		buttonPanel.setLocation(0, 60);
		buttonPanel.setSize(FRAME_WIDTH, 20);
		contentPane.add(buttonPanel);
		
		enterBtn = new JButton("Ready");
		buttonPanel.add(enterBtn);
		
		higherBtn = new JButton("Higher");
		lowerBtn = new JButton("Lower");
		correctBtn = new JButton("Correct!");
		contBtn = new JButton("I can do better");
		endBtn = new JButton("I can't handle that");
		guessBtn = new JButton("Guess");
		
		contentPane.setOpaque(true);
		return contentPane;
	}
	
	// updates the view to the main part of the game (Computer Guessing)
	
	public void initGame()
	{	
		frame.invalidate();
		
		buttonPanel.remove(enterBtn);
		buttonPanel.add(higherBtn);
		buttonPanel.add(correctBtn);
		buttonPanel.add(lowerBtn);
		
		contentPane.add(buttonPanel);
		
		frame.validate();
	}
	
	// Gives user the option to quit or challenge the computer
	
	public void initEndGame()
	{
		frame.invalidate();
		
		buttonPanel.remove(lowerBtn);
		buttonPanel.remove(higherBtn);
		buttonPanel.remove(correctBtn);
		
		buttonPanel.add(contBtn);
		buttonPanel.add(endBtn);
		
		frame.validate();
	}
	
	// updates the view to begin the user guessing part of the game
	
	public void initUserGame()
	{
		frame.invalidate();
		
		buttonPanel.remove(endBtn);
		buttonPanel.remove(contBtn);
		buttonPanel.add(guessBtn);
		textLabel.setText("I'm thinking of a number between 0 and 10000. What is it?");
		
		
		textPanel = new JPanel(new GridLayout(1,1));
		textPanel.setLocation(0, 35);
		textPanel.setSize(FRAME_WIDTH, 20);
		contentPane.add(textPanel);
		
		numGuesses = new JLabel("Guesses: ");
		textPanel.add(numGuesses);
		
		textBox = new JTextField();
		textPanel.add(textBox);
				
		frame.validate();
	}
	
	
	// sets up all the buttons
	public void addActionListeners(ActionListener listener)
	{	
		enterBtn.setActionCommand("Ready");
		higherBtn.setActionCommand("Higher");
		lowerBtn.setActionCommand("Lower");
		correctBtn.setActionCommand("Correct!");
		contBtn.setActionCommand("Continue");
		endBtn.setActionCommand("End");
		guessBtn.setActionCommand("Guess");
		
		enterBtn.addActionListener(listener);
		higherBtn.addActionListener(listener);
		lowerBtn.addActionListener(listener);
		correctBtn.addActionListener(listener);
		contBtn.addActionListener(listener);
		endBtn.addActionListener(listener);
		guessBtn.addActionListener(listener);
	}
	
	public void setText(String text)
	{
		textLabel.setText(text);
	}
	
	public void setGuesses(String text)
	{
		numGuesses.setText(text);
	}
	
	public String getGuess()
	{
		return textBox.getText();
	}
	
	public JFrame getFrame()
	{
		return frame;
	}
	
	public JPanel getContentPane()
	{
		return contentPane;
	}
	
	public JButton getEnterBtn()
	{
		return enterBtn;
	}
		
	public JButton getHigherBtn()
	{
		return higherBtn;
	}
	
	public JButton getLowerBtn()
	{
		return lowerBtn;
	}
	
	public JButton getCorrectBtn()
	{
		return correctBtn;
	}
}
